view: alert_status_counts {
  sql_table_name: cassandra.seceon_analytics.alert_status_counts ;;
  suggestions: no

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: alert_status {
    type: string
    sql: ${TABLE}.alert_status ;;
  }

  dimension: alert_status_count {
    type: number
    sql: ${TABLE}.alert_status_count ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,
      day_of_month,
      month_num
    ]
    sql: ${TABLE}.create_date ;;
  }

  dimension_group: search {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.search_date ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: is_open {
    type: yesno
    sql: ${alert_status} = 'OPEN' ;;
  }

  dimension: total_count_raw {
    type: number
    sql: (Select count(*) from cassandra.seceon_analytics.alert_status_counts) ;;
  }

  measure: total_count {
    type: max
    sql: ${total_count_raw} ;;
  }

  measure: count_open_alert_statuses {
    type: count
    filters: {
      field: is_open
      value: "yes"
    }
  }

  measure: open_alert_status_ratio {
    type: number
    sql: 1.0*${count_open_alert_statuses}/nullif(${count},0) ;;
    value_format_name: percent_1
  }


  measure: count {
    type: count
    drill_fields: [id]
  }
}
