view: applications {
  sql_table_name: cassandra.seceon_prov.applications ;;
  suggestions: no

  dimension_group: application_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_create_time ;;
  }

  dimension: application_description {
    type: string
    sql: ${TABLE}.application_description ;;
  }

  dimension_group: application_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_modify_time ;;
  }

  dimension: application_name {
    type: string
    sql: ${TABLE}.application_name ;;
  }

  dimension: application_port {
    type: number
    sql: ${TABLE}.application_port ;;
  }

  dimension: application_proto {
    type: number
    sql: ${TABLE}.application_proto ;;
  }

  dimension: application_tags {
    type: string
    sql: ${TABLE}.application_tags ;;
  }

  dimension: application_tags_string {
    type: string
    sql: ${TABLE}.application_tags_string ;;
  }

  measure: count {
    type: count
    drill_fields: [application_name]
  }
}
