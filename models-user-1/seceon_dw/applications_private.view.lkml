view: applications_private {
  sql_table_name: cassandra.seceon_prov.applications_private ;;
  suggestions: no

  dimension: application_asset_group {
    type: string
    sql: ${TABLE}.application_asset_group ;;
  }

  dimension_group: application_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_create_time ;;
  }

  dimension: application_description {
    type: string
    sql: ${TABLE}.application_description ;;
  }

  dimension: application_id {
    type: string
    sql: ${TABLE}.application_id ;;
  }

  dimension_group: application_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_modify_time ;;
  }

  dimension: application_name {
    type: string
    sql: ${TABLE}.application_name ;;
  }

  dimension: application_port {
    type: number
    sql: ${TABLE}.application_port ;;
  }

  dimension: application_proto {
    type: number
    sql: ${TABLE}.application_proto ;;
  }

  dimension: application_tags {
    type: string
    sql: ${TABLE}.application_tags ;;
  }

  dimension: application_tags_string {
    type: string
    sql: ${TABLE}.application_tags_string ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: [application_name]
  }
}
