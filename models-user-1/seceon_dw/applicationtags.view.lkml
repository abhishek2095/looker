view: applicationtags {
  sql_table_name: cassandra.seceon_prov.applicationtags ;;
  suggestions: no

  dimension_group: application_tag_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_tag_create_time ;;
  }

  dimension: application_tag_description {
    type: string
    sql: ${TABLE}.application_tag_description ;;
  }

  dimension: application_tag_id {
    type: number
    sql: ${TABLE}.application_tag_id ;;
  }

  dimension_group: application_tag_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_tag_modify_time ;;
  }

  dimension: application_tag_name {
    type: string
    sql: ${TABLE}.application_tag_name ;;
  }

  measure: count {
    type: count
    drill_fields: [application_tag_name]
  }
}
