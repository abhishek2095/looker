view: applicationtypes {
  sql_table_name: cassandra.seceon_prov.applicationtypes ;;
  suggestions: no

  dimension_group: application_type_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_type_create_time ;;
  }

  dimension: application_type_id {
    type: number
    sql: ${TABLE}.application_type_id ;;
  }

  dimension_group: application_type_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.application_type_modify_time ;;
  }

  dimension: application_type_name {
    type: string
    sql: ${TABLE}.application_type_name ;;
  }

  dimension: assignable {
    type: number
    sql: ${TABLE}.assignable ;;
  }

  measure: count {
    type: count
    drill_fields: [application_type_name]
  }
}
