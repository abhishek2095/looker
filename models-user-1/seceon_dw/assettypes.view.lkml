view: assettypes {
  sql_table_name: cassandra.seceon_prov.assettypes ;;
  suggestions: no

  dimension: assettype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.assettype_id ;;
  }

  dimension: assettype_name {
    type: string
    sql: ${TABLE}.assettype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [assettype_id, assettype_name]
  }
}
