view: auto_remediation_settings {
  sql_table_name: cassandra.seceon_prov.auto_remediation_settings ;;
  suggestions: no

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: alert_type_list {
    type: string
    sql: ${TABLE}.alert_type_list ;;
  }

  dimension: asset_group_list {
    type: string
    sql: ${TABLE}.asset_group_list ;;
  }

  dimension: confidence_score {
    type: number
    sql: ${TABLE}.confidence_score ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_time ;;
  }

  dimension: device_list {
    type: string
    sql: ${TABLE}.device_list ;;
  }

  dimension: enabled {
    type: number
    sql: ${TABLE}.enabled ;;
  }

  dimension: friday {
    type: string
    sql: ${TABLE}.friday ;;
  }

  dimension_group: friday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.friday_schedule_from ;;
  }

  dimension: friday_schedule_from_time {
    type: string
    sql: ${TABLE}.friday_schedule_from_time ;;
  }

  dimension_group: friday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.friday_schedule_until ;;
  }

  dimension: friday_schedule_until_time {
    type: string
    sql: ${TABLE}.friday_schedule_until_time ;;
  }

  dimension_group: modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modify_time ;;
  }

  dimension: moment_timezone {
    type: string
    sql: ${TABLE}.moment_timezone ;;
  }

  dimension: monday {
    type: string
    sql: ${TABLE}.monday ;;
  }

  dimension_group: monday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.monday_schedule_from ;;
  }

  dimension: monday_schedule_from_time {
    type: string
    sql: ${TABLE}.monday_schedule_from_time ;;
  }

  dimension_group: monday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.monday_schedule_until ;;
  }

  dimension: monday_schedule_until_time {
    type: string
    sql: ${TABLE}.monday_schedule_until_time ;;
  }

  dimension: saturday {
    type: string
    sql: ${TABLE}.saturday ;;
  }

  dimension_group: saturday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.saturday_schedule_from ;;
  }

  dimension: saturday_schedule_from_time {
    type: string
    sql: ${TABLE}.saturday_schedule_from_time ;;
  }

  dimension_group: saturday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.saturday_schedule_until ;;
  }

  dimension: saturday_schedule_until_time {
    type: string
    sql: ${TABLE}.saturday_schedule_until_time ;;
  }

  dimension: severity_list {
    type: string
    sql: ${TABLE}.severity_list ;;
  }

  dimension: sunday {
    type: string
    sql: ${TABLE}.sunday ;;
  }

  dimension_group: sunday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sunday_schedule_from ;;
  }

  dimension: sunday_schedule_from_time {
    type: string
    sql: ${TABLE}.sunday_schedule_from_time ;;
  }

  dimension_group: sunday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sunday_schedule_until ;;
  }

  dimension: sunday_schedule_until_time {
    type: string
    sql: ${TABLE}.sunday_schedule_until_time ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: thursday {
    type: string
    sql: ${TABLE}.thursday ;;
  }

  dimension_group: thursday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.thursday_schedule_from ;;
  }

  dimension: thursday_schedule_from_time {
    type: string
    sql: ${TABLE}.thursday_schedule_from_time ;;
  }

  dimension_group: thursday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.thursday_schedule_until ;;
  }

  dimension: thursday_schedule_until_time {
    type: string
    sql: ${TABLE}.thursday_schedule_until_time ;;
  }

  dimension: timezone_offset {
    type: number
    sql: ${TABLE}.timezone_offset ;;
  }

  dimension: tuesday {
    type: string
    sql: ${TABLE}.tuesday ;;
  }

  dimension_group: tuesday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.tuesday_schedule_from ;;
  }

  dimension: tuesday_schedule_from_time {
    type: string
    sql: ${TABLE}.tuesday_schedule_from_time ;;
  }

  dimension_group: tuesday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.tuesday_schedule_until ;;
  }

  dimension: tuesday_schedule_until_time {
    type: string
    sql: ${TABLE}.tuesday_schedule_until_time ;;
  }

  dimension: wednesday {
    type: string
    sql: ${TABLE}.wednesday ;;
  }

  dimension_group: wednesday_schedule_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wednesday_schedule_from ;;
  }

  dimension: wednesday_schedule_from_time {
    type: string
    sql: ${TABLE}.wednesday_schedule_from_time ;;
  }

  dimension_group: wednesday_schedule_until {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wednesday_schedule_until ;;
  }

  dimension: wednesday_schedule_until_time {
    type: string
    sql: ${TABLE}.wednesday_schedule_until_time ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
