view: blacklisted {
  sql_table_name: cassandra.seceon_prov.blacklisted ;;
  suggestions: no

  dimension_group: blacklist_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.blacklist_create_time ;;
  }

  dimension: blacklist_direction {
    type: string
    sql: ${TABLE}.blacklist_direction ;;
  }

  dimension: blacklist_entity {
    type: string
    sql: ${TABLE}.blacklist_entity ;;
  }

  dimension: blacklist_id {
    type: string
    sql: ${TABLE}.blacklist_id ;;
  }

  dimension_group: blacklist_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.blacklist_modify_time ;;
  }

  dimension: blacklist_notes {
    type: string
    sql: ${TABLE}.blacklist_notes ;;
  }

  dimension: blacklist_source {
    type: string
    sql: ${TABLE}.blacklist_source ;;
  }

  dimension: blacklist_type {
    type: number
    sql: ${TABLE}.blacklist_type ;;
  }

  dimension: blacklist_value {
    type: string
    sql: ${TABLE}.blacklist_value ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
