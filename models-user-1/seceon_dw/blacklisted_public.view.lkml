view: blacklisted_public {
  sql_table_name: cassandra.seceon_prov.blacklisted_public ;;
  suggestions: no

  dimension_group: added {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.added_time ;;
  }

  dimension: blacklist_count {
    type: string
    sql: ${TABLE}.blacklist_count ;;
  }

  dimension_group: blacklist {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.blacklist_date ;;
  }

  dimension: blacklist_direction {
    type: string
    sql: ${TABLE}.blacklist_direction ;;
  }

  dimension: blacklist_entity {
    type: string
    sql: ${TABLE}.blacklist_entity ;;
  }

  dimension: blacklist_notes {
    type: string
    sql: ${TABLE}.blacklist_notes ;;
  }

  dimension: blacklist_source {
    type: string
    sql: ${TABLE}.blacklist_source ;;
  }

  dimension: blacklist_type {
    type: string
    sql: ${TABLE}.blacklist_type ;;
  }

  dimension: percentage {
    type: string
    sql: ${TABLE}.percentage ;;
  }

  dimension: source_count {
    type: string
    sql: ${TABLE}.source_count ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
