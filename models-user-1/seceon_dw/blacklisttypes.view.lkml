view: blacklisttypes {
  sql_table_name: cassandra.seceon_prov.blacklisttypes ;;
  suggestions: no

  dimension: blacklisttype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.blacklisttype_id ;;
  }

  dimension: blacklisttype_name {
    type: string
    sql: ${TABLE}.blacklisttype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [blacklisttype_id, blacklisttype_name]
  }
}
