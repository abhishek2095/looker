view: connection_policies {
  sql_table_name: cassandra.seceon_prov.connection_policies ;;
  suggestions: no

  dimension: connection_policy_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.connection_policy_id ;;
  }

  dimension: connection_status {
    type: number
    sql: ${TABLE}.connection_status ;;
  }

  dimension: connection_type {
    type: number
    sql: ${TABLE}.connection_type ;;
  }

  dimension: destination_entity {
    type: string
    sql: ${TABLE}.destination_entity ;;
  }

  dimension: destination_type {
    type: number
    sql: ${TABLE}.destination_type ;;
  }

  dimension: except_flag {
    type: number
    sql: ${TABLE}.except_flag ;;
  }

  dimension: policy_type {
    type: number
    sql: ${TABLE}.policy_type ;;
  }

  dimension: source_entity {
    type: string
    sql: ${TABLE}.source_entity ;;
  }

  dimension: source_type {
    type: number
    sql: ${TABLE}.source_type ;;
  }

  measure: count {
    type: count
    drill_fields: [connection_policy_id]
  }
}
