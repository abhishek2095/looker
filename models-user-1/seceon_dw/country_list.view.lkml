view: country_list {
  sql_table_name: cassandra.seceon_prov.country_list ;;
  suggestions: no

  dimension: country_id {
    type: string
    sql: ${TABLE}.country_id ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  measure: count {
    type: count
    drill_fields: [country_name]
  }
}
