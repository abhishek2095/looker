view: data_retention_config {
  sql_table_name: cassandra.seceon_prov.data_retention_config ;;
  suggestions: no

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_date ;;
  }

  dimension: data_detail_set {
    type: string
    sql: ${TABLE}.data_detail_set ;;
  }

  dimension: data_kind {
    type: string
    sql: ${TABLE}.data_kind ;;
  }

  dimension: data_retention_days {
    type: number
    sql: ${TABLE}.data_retention_days ;;
  }

  dimension: is_auto_rollover_enable {
    type: yesno
    sql: ${TABLE}.is_auto_rollover_enable ;;
  }

  dimension: is_user_configurable {
    type: yesno
    sql: ${TABLE}.is_user_configurable ;;
  }

  dimension: max_data_retention_days_limit {
    type: number
    sql: ${TABLE}.max_data_retention_days_limit ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension_group: update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_date ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
