view: device_types {
  sql_table_name: cassandra.seceon_prov.device_types ;;
  suggestions: no

  dimension: device_category_name {
    type: string
    sql: ${TABLE}.device_category_name ;;
  }

  dimension: devicetype_id {
    type: number
    sql: ${TABLE}.devicetype_id ;;
  }

  dimension: devicetype_name {
    type: string
    sql: ${TABLE}.devicetype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [device_category_name, devicetype_name]
  }
}
