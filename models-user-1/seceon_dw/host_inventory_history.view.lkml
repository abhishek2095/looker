view: host_inventory_history {
  sql_table_name: cassandra.seceon_prov.host_inventory_history ;;
  suggestions: no

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_time ;;
  }

  dimension_group: first_seen {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_seen_time ;;
  }

  dimension: host_ip {
    type: string
    sql: ${TABLE}.host_ip ;;
  }

  dimension: host_name {
    type: string
    sql: ${TABLE}.host_name ;;
  }

  dimension_group: last_seen {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_seen_time ;;
  }

  dimension: services {
    type: string
    sql: ${TABLE}.services ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: [host_name]
  }
}
