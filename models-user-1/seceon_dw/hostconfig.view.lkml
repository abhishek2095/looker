view: hostconfig {
  sql_table_name: cassandra.seceon_prov.hostconfig ;;
  suggestions: no

  dimension: hostconfig_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.hostconfig_id ;;
  }

  dimension: entity_name {
    type: string
    sql: ${TABLE}.entity_name ;;
  }

  dimension: entity_notes {
    type: string
    sql: ${TABLE}.entity_notes ;;
  }

  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }

  dimension: entity_value {
    type: string
    sql: ${TABLE}.entity_value ;;
  }

  dimension_group: host_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.host_create_time ;;
  }

  dimension: host_flag {
    type: number
    sql: ${TABLE}.host_flag ;;
  }

  dimension_group: host_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.host_modify_time ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: user_name {
    type: string
    sql: ${TABLE}.user_name ;;
  }

  measure: count {
    type: count
    drill_fields: [hostconfig_id, entity_name, user_name]
  }
}
