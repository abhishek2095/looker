view: ipv4_reserved {
  sql_table_name: cassandra.seceon_prov.ipv4_reserved ;;
  suggestions: no

  dimension: ipv4_reserved_cidr {
    type: number
    sql: ${TABLE}.ipv4_reserved_cidr ;;
  }

  dimension: ipv4_reserved_network {
    type: string
    sql: ${TABLE}.ipv4_reserved_network ;;
  }

  dimension: ipv4_reserved_range {
    type: string
    sql: ${TABLE}.ipv4_reserved_range ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
