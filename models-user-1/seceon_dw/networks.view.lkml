view: networks {
  sql_table_name: cassandra.seceon_prov.networks ;;
  suggestions: no

  dimension: network_id {
    primary_key: yes
    type: string
    sql: ${TABLE}.network_id ;;
  }

  dimension: network_authorized {
    type: string
    sql: ${TABLE}.network_authorized ;;
  }

  dimension: network_authorized_string {
    type: string
    sql: ${TABLE}.network_authorized_string ;;
  }

  dimension_group: network_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.network_create_time ;;
  }

  dimension_group: network_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.network_modify_time ;;
  }

  dimension: network_name {
    type: string
    sql: ${TABLE}.network_name ;;
  }

  dimension: network_order {
    type: number
    sql: ${TABLE}.network_order ;;
  }

  dimension: network_tags {
    type: string
    sql: ${TABLE}.network_tags ;;
  }

  dimension: network_tags_string {
    type: string
    sql: ${TABLE}.network_tags_string ;;
  }

  dimension: network_value {
    type: string
    sql: ${TABLE}.network_value ;;
  }

  dimension: networktype_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.networktype_id ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: [network_id, network_name, networktypes.networktype_name, networktypes.networktype_id]
  }
}
