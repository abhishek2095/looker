view: networktags {
  sql_table_name: cassandra.seceon_prov.networktags ;;
  suggestions: no

  dimension: networktag_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.networktag_id ;;
  }

  dimension: networktag_description {
    type: string
    sql: ${TABLE}.networktag_description ;;
  }

  dimension: networktag_name {
    type: string
    sql: ${TABLE}.networktag_name ;;
  }

  measure: count {
    type: count
    drill_fields: [networktag_id, networktag_name]
  }
}
