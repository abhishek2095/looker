view: networktypes {
  sql_table_name: cassandra.seceon_prov.networktypes ;;
  suggestions: no

  dimension: networktype_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.networktype_id ;;
  }

  dimension: networktype_name {
    type: string
    sql: ${TABLE}.networktype_name ;;
  }

  measure: count {
    type: count
    drill_fields: [networktype_id, networktype_name, networks.count]
  }
}
