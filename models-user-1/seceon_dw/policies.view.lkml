view: policies {
  sql_table_name: cassandra.seceon_prov.policies ;;
  suggestions: no

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: active {
    type: yesno
    sql: ${TABLE}.active ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_date ;;
  }

  dimension: dst {
    type: string
    sql: ${TABLE}.dst ;;
  }

  dimension: dst_app_env {
    type: string
    sql: ${TABLE}.dst_app_env ;;
  }

  dimension: dst_app_env_type {
    type: number
    sql: ${TABLE}.dst_app_env_type ;;
  }

  dimension: dst_file_dir_env {
    type: string
    sql: ${TABLE}.dst_file_dir_env ;;
  }

  dimension: dst_file_dir_env_type {
    type: number
    sql: ${TABLE}.dst_file_dir_env_type ;;
  }

  dimension: dst_type {
    type: number
    sql: ${TABLE}.dst_type ;;
  }

  dimension_group: modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modify_date ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: policy_type {
    type: number
    sql: ${TABLE}.policy_type ;;
  }

  dimension: src {
    type: string
    sql: ${TABLE}.src ;;
  }

  dimension: src_type {
    type: number
    sql: ${TABLE}.src_type ;;
  }

  dimension: tags {
    type: string
    sql: ${TABLE}.tags ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
