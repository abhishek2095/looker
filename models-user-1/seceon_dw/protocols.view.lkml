view: protocols {
  sql_table_name: cassandra.seceon_prov.protocols ;;
  suggestions: no

  dimension: proto_keyword {
    type: string
    sql: ${TABLE}.proto_keyword ;;
  }

  dimension: proto_name {
    type: string
    sql: ${TABLE}.proto_name ;;
  }

  dimension: proto_number {
    type: number
    sql: ${TABLE}.proto_number ;;
  }

  measure: count {
    type: count
    drill_fields: [proto_name]
  }
}
