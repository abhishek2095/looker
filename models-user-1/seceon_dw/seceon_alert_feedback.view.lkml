view: seceon_alert_feedback {
  sql_table_name: cassandra.seceon_prov.seceon_alert_feedback ;;
  suggestions: no

  dimension: alert_type_id {
    type: string
    sql: ${TABLE}.alert_type_id ;;
  }

  dimension: object_id {
    type: string
    sql: ${TABLE}.object_id ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: ttl {
    type: number
    sql: ${TABLE}.ttl ;;
  }

  dimension: user_name {
    type: string
    sql: ${TABLE}.user_name ;;
  }

  measure: count {
    type: count
    drill_fields: [user_name]
  }
}
