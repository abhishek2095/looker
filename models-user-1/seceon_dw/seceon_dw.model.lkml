connection: "presto"

# include all the views
include: "*.view"

# include all the dashboards
include: "*.dashboard"

datagroup: seceon_dw_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: seceon_dw_default_datagroup

explore: applications {}

explore: applications_private {}

explore: applicationtags {}

explore: applicationtypes {}

explore: assettypes {}

explore: auto_remediation_settings {}

explore: blacklisted {}

explore: blacklisted_public {}

explore: blacklisttypes {}

explore: connection_policies {}

explore: country_list {}

explore: data_retention_config {}

explore: device_config {}

explore: device_types {}

explore: host_inventory {}

explore: host_inventory_history {}

explore: host_inventory_service_mapping {}

explore: hostconfig {}

explore: ipv4_reserved {}

explore: networks {
  join: networktypes {
    type: left_outer
    sql_on: ${networks.networktype_id} = ${networktypes.networktype_id} ;;
    relationship: many_to_one
  }
}

explore: networktags {}

explore: networktypes {}

explore: policies {}

explore: protocols {}

explore: running_services_status {}

explore: seceon_alert_feedback {}

explore: syslog_configuration {}

explore: system_alert_config {}

explore: system_backup_configuration {}

explore: system_backup_history {}

explore: system_threshold {}

explore: tcpflags {}

explore: ticonfig {}

explore: tihistory {}

explore: trusted_domain {}

explore: trusted_list {}

explore: validtos {}

explore: whitelist_assets {}

explore: whitelist_connections {}
