view: system_backup_configuration {
  sql_table_name: cassandra.seceon_prov.system_backup_configuration ;;
  suggestions: no

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: dest_host {
    type: string
    sql: ${TABLE}.dest_host ;;
  }

  dimension: dest_password {
    type: string
    sql: ${TABLE}.dest_password ;;
  }

  dimension: dest_path {
    type: string
    sql: ${TABLE}.dest_path ;;
  }

  dimension: dest_port {
    type: number
    sql: ${TABLE}.dest_port ;;
  }

  dimension: dest_type {
    type: string
    sql: ${TABLE}.dest_type ;;
  }

  dimension: dest_username {
    type: string
    sql: ${TABLE}.dest_username ;;
  }

  dimension: enable_backup_feature {
    type: yesno
    sql: ${TABLE}.enable_backup_feature ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension_group: time_of_backup {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.time_of_backup ;;
  }

  measure: count {
    type: count
    drill_fields: [id, dest_username]
  }
}
