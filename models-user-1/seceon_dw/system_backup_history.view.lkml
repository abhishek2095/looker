view: system_backup_history {
  sql_table_name: cassandra.seceon_prov.system_backup_history ;;
  suggestions: no

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: action_performed {
    type: string
    sql: ${TABLE}.action_performed ;;
  }

  dimension_group: backup_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.backup_end_date ;;
  }

  dimension_group: backup_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.backup_start_date ;;
  }

  dimension: backup_status {
    type: string
    sql: ${TABLE}.backup_status ;;
  }

  dimension: cs_backup_file {
    type: string
    sql: ${TABLE}.cs_backup_file ;;
  }

  dimension: dest_addr {
    type: string
    sql: ${TABLE}.dest_addr ;;
  }

  dimension: es_backup_file {
    type: string
    sql: ${TABLE}.es_backup_file ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: user {
    type: string
    sql: ${TABLE}.user ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
