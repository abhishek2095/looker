view: system_threshold {
  sql_table_name: cassandra.seceon_prov.system_threshold ;;
  suggestions: no

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: critical_th {
    type: string
    sql: ${TABLE}.critical_th ;;
  }

  dimension: major_th {
    type: string
    sql: ${TABLE}.major_th ;;
  }

  dimension: minor_th {
    type: string
    sql: ${TABLE}.minor_th ;;
  }

  dimension: system_event_type {
    type: string
    sql: ${TABLE}.system_event_type ;;
  }

  dimension: system_event_type_id {
    type: string
    sql: ${TABLE}.system_event_type_id ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
