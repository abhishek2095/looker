view: tcpflags {
  sql_table_name: cassandra.seceon_prov.tcpflags ;;
  suggestions: no

  dimension: tcpflags_bit {
    type: number
    sql: ${TABLE}.tcpflags_bit ;;
  }

  dimension: tcpflags_letter {
    type: string
    sql: ${TABLE}.tcpflags_letter ;;
  }

  dimension: tcpflags_str {
    type: string
    sql: ${TABLE}.tcpflags_str ;;
  }

  dimension: tcpflags_val {
    type: number
    sql: ${TABLE}.tcpflags_val ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
