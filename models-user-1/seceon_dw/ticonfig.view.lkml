view: ticonfig {
  sql_table_name: cassandra.seceon_prov.ticonfig ;;
  suggestions: no

  dimension_group: daytime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.daytime ;;
  }

  dimension: proxy_auth {
    type: yesno
    sql: ${TABLE}.proxy_auth ;;
  }

  dimension: proxy_port {
    type: number
    sql: ${TABLE}.proxy_port ;;
  }

  dimension: proxy_server {
    type: string
    sql: ${TABLE}.proxy_server ;;
  }

  dimension: proxy_server_enable {
    type: yesno
    sql: ${TABLE}.proxy_server_enable ;;
  }

  dimension: proxy_server_password {
    type: string
    sql: ${TABLE}.proxy_server_password ;;
  }

  dimension: proxy_server_username {
    type: string
    sql: ${TABLE}.proxy_server_username ;;
  }

  dimension: server_ip {
    type: string
    sql: ${TABLE}.server_ip ;;
  }

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: ti_id {
    type: string
    sql: ${TABLE}.ti_id ;;
  }

  measure: count {
    type: count
    drill_fields: [proxy_server_username]
  }
}
