view: tihistory {
  sql_table_name: cassandra.seceon_prov.tihistory ;;
  suggestions: no

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: ti_action {
    type: string
    sql: ${TABLE}.ti_action ;;
  }

  dimension: ti_action_result {
    type: string
    sql: ${TABLE}.ti_action_result ;;
  }

  dimension: ti_action_result_message {
    type: string
    sql: ${TABLE}.ti_action_result_message ;;
  }

  dimension: ti_action_user {
    type: string
    sql: ${TABLE}.ti_action_user ;;
  }

  dimension_group: ti_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ti_end_time ;;
  }

  dimension: ti_log_id {
    type: string
    sql: ${TABLE}.ti_log_id ;;
  }

  dimension_group: ti_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ti_start_time ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
