view: trusted_domain {
  sql_table_name: cassandra.seceon_prov.trusted_domain ;;
  suggestions: no

  dimension: domain {
    type: string
    sql: ${TABLE}.domain ;;
  }

  dimension: iprange {
    type: string
    sql: ${TABLE}.iprange ;;
  }

  dimension_group: trusted_domain_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.trusted_domain_create_time ;;
  }

  dimension_group: trusted_domain_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.trusted_domain_modify_time ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
