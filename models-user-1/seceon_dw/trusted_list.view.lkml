view: trusted_list {
  sql_table_name: cassandra.seceon_prov.trusted_list ;;
  suggestions: no

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension_group: trustlist_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.trustlist_create_time ;;
  }

  dimension: trustlist_id {
    type: string
    sql: ${TABLE}.trustlist_id ;;
  }

  dimension_group: trustlist_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.trustlist_modify_time ;;
  }

  dimension: trustlist_notes {
    type: string
    sql: ${TABLE}.trustlist_notes ;;
  }

  dimension: trustlist_type {
    type: number
    sql: ${TABLE}.trustlist_type ;;
  }

  dimension: trustlist_value {
    type: string
    sql: ${TABLE}.trustlist_value ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
