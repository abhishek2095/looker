view: validtos {
  sql_table_name: cassandra.seceon_prov.validtos ;;
  suggestions: no

  dimension: tos_code_point {
    type: string
    sql: ${TABLE}.tos_code_point ;;
  }

  dimension: tos_diffserv {
    type: number
    sql: ${TABLE}.tos_diffserv ;;
  }

  dimension: tos_name {
    type: string
    sql: ${TABLE}.tos_name ;;
  }

  dimension: tos_notes {
    type: string
    sql: ${TABLE}.tos_notes ;;
  }

  measure: count {
    type: count
    drill_fields: [tos_name]
  }
}
