view: whitelist_assets {
  sql_table_name: cassandra.seceon_prov.whitelist_assets ;;
  suggestions: no

  dimension: wla_asset_address {
    type: string
    sql: ${TABLE}.wla_asset_address ;;
  }

  dimension: wla_asset_type_id {
    type: number
    sql: ${TABLE}.wla_asset_type_id ;;
  }

  dimension: wla_id {
    type: string
    sql: ${TABLE}.wla_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
