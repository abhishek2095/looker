view: whitelist_connections {
  sql_table_name: cassandra.seceon_prov.whitelist_connections ;;
  suggestions: no

  dimension: tenant_id {
    type: string
    sql: ${TABLE}.tenant_id ;;
  }

  dimension: wlcon_app_name {
    type: string
    sql: ${TABLE}.wlcon_app_name ;;
  }

  dimension_group: wlcon_create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wlcon_create_time ;;
  }

  dimension: wlcon_dst_name {
    type: string
    sql: ${TABLE}.wlcon_dst_name ;;
  }

  dimension: wlcon_dst_network_id {
    type: string
    sql: ${TABLE}.wlcon_dst_network_id ;;
  }

  dimension: wlcon_id {
    type: string
    sql: ${TABLE}.wlcon_id ;;
  }

  dimension_group: wlcon_modify {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wlcon_modify_time ;;
  }

  dimension: wlcon_port {
    type: number
    sql: ${TABLE}.wlcon_port ;;
  }

  dimension: wlcon_protocol {
    type: number
    sql: ${TABLE}.wlcon_protocol ;;
  }

  dimension: wlcon_src_name {
    type: string
    sql: ${TABLE}.wlcon_src_name ;;
  }

  dimension: wlcon_src_network_id {
    type: string
    sql: ${TABLE}.wlcon_src_network_id ;;
  }

  dimension: wlcon_status {
    type: number
    sql: ${TABLE}.wlcon_status ;;
  }

  measure: count {
    type: count
    drill_fields: [wlcon_src_name, wlcon_dst_name, wlcon_app_name]
  }
}
